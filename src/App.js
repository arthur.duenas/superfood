import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter } from "react-router-dom";
import Routes from "./routes";
import Headbar from "./components/headbar";
import Footer from "./components/footer";

function App() {
  return (
    <BrowserRouter>
      <Headbar />
      <Routes />
      <Footer />
    </BrowserRouter>
  );
}

export default App;
