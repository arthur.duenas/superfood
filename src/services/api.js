import axios from "axios";

const api = axios.create({
  // baseURL: "http://localhost:3333",
  baseURL: "http://18.228.49.63:3340",
});

api.interceptors.request.use(async (config) => {
  const token = localStorage.getItem("token");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default api;
